# Boolean transformations

Count Zhegalkin polynomial, fourier coefficients by truth table.

# How to use

1. Open `main.py` to edit;
2. Put decimal vector of your functions in `dec_func` variable;
3. Save file;
4. Run `main.py` script by python:

```shell
python main.py
```
